#+OPTIONS: num:nil
#+TITLE: 人類進化の時代／段階区分
#+SUBTITLE: 自然人類学：第3回の2
#+AUTHOR: 担当：竹ノ下祐二
#+EMAIL: y-takenoshita@ous.ac.jp
#+DATE: 2024/05/07
#+REVEAL_MISCINFO: 岡山理科大学理学部動物学科2024年度春学期開講科目
#+REVEAL_MULTIPLEX_ID: 71ca6ccb7a8a582a
#+REVEAL_MULTIPLEX_SECRET: 17149843406344521200
* 人類進化の5段階
:PROPERTIES:
:REVEAL_EXTRA_ATTR: class="onlyheading"
:END:

** 人類の進化系統樹

#+CAPTION: 斎藤ほか 「ヒトの進化」(岩波書店, 2006年) より
[[file:img/hominid-phylogeny.png]]


** おおまかな時代区分 (馬場, 2014)
1. 初期猿人 (700-400万年前) 
2. 猿人 (400-220万年前)
3. 原人 (220-90万年前)
4. 旧人 (90-20万年前)
5. 新人 (20万年前-現在)

   
** 外見上の変化
#+CAPTION: 馬場, 2014
[[file:img/humanevolution-5stages.png]]


** 分布域の拡大
#+CAPTION: 馬場, 2014
[[file:img/human-distribution-5stages.png]]


** 人間らしさの4要素の出現機序
#+ATTR_HTML: :width 600
#+Caption: 馬場, 2015
[[file:img/human4characteristics.png]]
