#+OPTIONS: num:nil
#+TITLE: 捕食者の戦略、被捕食者の戦略
#+SUBTITLE: 生態学：第7回の2
#+AUTHOR: 担当：竹ノ下祐二
#+EMAIL: y-takenoshita@ous.ac.jp
#+DATE: 2024/05/31
#+REVEAL_MISCINFO: 岡山理科大学理学部動物学科2024年度春学期開講科目
#+REVEAL_MULTIPLEX_ID: 71e1e647ef1a2765
#+REVEAL_MULTIPLEX_SECRET: 17170437700063150692
* 捕食者の戦略
:PROPERTIES:
:REVEAL_EXTRA_ATTR: class="onlyheading"
:END:

** 最適採食戦略 (optimal foraging strategy)
- 餌生物の選択 :: ジェネラリスト（雑食者）, スペシャリスト（果実食、昆虫食、草食、葉食など；種レベルで特殊化することも）
- 採食様式の選択 :: 高コスト-高栄養 vs 低コスト-低栄養 など


* 被捕食者の戦略
:PROPERTIES:
:REVEAL_EXTRA_ATTR: class="onlyheading"
:END:

** 対捕食者戦略 (antipredatory strategy)
- 物理的防御 :: トゲ、ハリ、角、堅い殻、隠蔽色、擬態など
- 化学的防御 :: タンニン、アルカロイドなどの二次化合物
- 行動的防御 :: 捕食を免れるためにどのような行動ができるだろうか？

** グループワーク
- /アフリカの熱帯林に生息する架空のサル/ になったつもりで、捕食を免れるためにどのような行動ができるか意見を出し合ってみよう
  - サル自身の食性は、植物を中心とした雑食（果実、種子、新葉、草、昆虫）
  - 基本的に樹上性：0m〜30mの高さを利用する
  - 体のサイズはニホンザルよりやや小ぶり（頭胴長50〜70cm、体重5〜8kg）
  - 捕食者は：ヒョウ、サルクイワシ、チンパンジー、ヒト（銃猟とわな猟）


** イメージをつかむために

#+REVEAL_HTML: <iframe width="560" height="315" src="https://www.youtube.com/embed/UYnJisF7Idc?si=_o20_OW5ycLwLa6x" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

** イメージをつかむために

#+REVEAL_HTML: <iframe width="560" height="315" src="https://www.youtube.com/embed/ZZtbW8Rb9C0?si=op4jarRvJ6_ny3aS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


** イメージをつかむために

#+REVEAL_HTML: <iframe width="560" height="315" src="https://www.youtube.com/embed/5i3eU-DyPzw?si=_N11RojrY7Ql6-hQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
