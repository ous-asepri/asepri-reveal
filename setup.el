;; Set the package installation directory so that packages aren't stored in the
;; ~/.emacs.d/elpa path.
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)

(setq package-user-dir (expand-file-name "./.packages"))
(setq package-archives '(("nongnu" . "https://elpa.nongnu.org/nongnu/")
                         ("melpa" . "https://melpa.org/packages/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(package-refresh-contents)

;; Install dependencies
(use-package htmlize :ensure t)

(use-package org :ensure t require t
  :custom
  ;; basic export settings
  (org-export-with-smart-quotes t)
  (org-export-with-broken-links 'mark)
  (org-export-with-toc nil)
  (org-export-allow-bind-keywords t)

  ;; basic html export settings
  (org-html-doctype "html5")
  (org-html-html5-fancy t)
  (org-html-validation-link nil)  
  (org-html-htmlize-output-type 'css)
  (org-html-head-include-scripts nil)
  (org-html-head-include-default-style nil)

  ;; org to html markup conversion settings
  (org-html-text-markup-alist
   '((bold . "<strong>%s</strong>")
     (code . "<code>%s</code>")
     (italic . "<em>%s</em>")
     (strike-through . "<del>%s</del>")
     (underline . "<span class=\"underline\">%s</span>")
     (verbatim . "<code>%s</code>")))
  :config
  (require 'ox-publish))

(use-package citeproc :ensure t)

(use-package citeproc-org :ensure t)

(use-package org-re-reveal :ensure t
  :custom
  ;; reveal.js location URL (or path)
  (org-re-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js@4.2.0/")

  (org-re-reveal-revealjs-version "4")  ;; reveal.js version

  ;; initialization options
  (org-re-reveal-height 720)
  (org-re-reveal-width 1280)
  (org-re-reveal-max-scale "2.0")
  (org-re-reveal-min-scale "0.2")
  (org-re-reveal-margin "0.05")
  (org-re-reveal-center nil)
  (org-re-reveal-transition "convex")
  (org-re-reveal-extra-options
   "coursemod: { enabled: true, shown: false }, chalkboard: {}, customcontrols: { controls: [ { icon: '<i class=\"fa fa-pen-square\"></i>', title: 'Toggle chalkboard (B)', action: 'RevealChalkboard.toggleChalkboard();' }, { icon: '<i class=\"fa fa-pen\"></i>', title: 'Toggle notes canvas (C)', action: 'RevealChalkboard.toggleNotesCanvas();' } ] }")


  ;; text highlighting
  (org-re-reveal-highlight-url "https://cdn.jsdelivr.net/npm/reveal.js@latest/plugin/highlight/highlight.js")
  (org-re-reveal-highlight-css "%r/plugin/highlight/zenburn.css")

  ;; multiplexing
  (org-re-reveal-multiplex-js-url "https://cdn.jsdelivr.net/npm/reveal-multiplex@0.1.0/")
  (org-re-reveal-multiplex-url "https://reveal-multiplex.glitch.me/")
  (org-re-reveal-multiplex-socketio-url "https://reveal-multiplex.glitch.me/socket.io/socket.io.js")
  (org-re-reveal-multiplex-client-ext "")

  ;; theme and slide layout 
  (org-re-reveal-theme "white") ;; theme
  (org-re-reveal-title-slide "<div class=\"title-grid\"><h1 class=\"title\">%t</h1><h2 class=\"subtitle\">&mdash;%s&mdash;</h2><p class=\"miscinfo\">%m</p><h2 class=\"author\">%a</h2><p class=\"e-mail\">%e</p><p class=\"date\">%d</p></div>") ;; title slide layout
  ;; (org-re-reveal-slide-container "<div class=\"content-grid\">%s</div>") ;; div for slide contents area
  (org-re-reveal-slide-grid-div "<div class=\"slide-grid\">") ;; div for whole slide including title and header/footer

  ;; plugin configuration
  (org-re-reveal-extra-scripts
      '("https://cdn.jsdelivr.net/npm/reveal.js-plugins@latest/audio-slideshow/RecordRTC.js"
        "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/js/all.min.js"))
  (org-re-reveal-plugins '(multiplex markdown zoom notes highlight menu audioslideshow audio-recorder customcontrols chalkboard))
  (org-re-reveal-external-plugins '((coursemod . "{ src: '/plugin/coursemod/coursemod.js', async: true }")))

  :config
  ;; add org-re-reveal to org-export-backends
  (add-to-list 'org-export-backends 're-reveal)

  ;; extra css
  (setq org-re-reveal-extra-css "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css
https://cdn.jsdelivr.net/npm/reveal.js-plugins@latest/customcontrols/style.css
https://cdn.jsdelivr.net/npm/reveal.js-plugins@latest/chalkboard/style.css
/css/reveal-extra.css")

  ;; plugin configuration
  (add-to-list 'org-re-reveal-plugin-config
               '(menu "RevealMenu" "https://cdn.jsdelivr.net/npm/reveal.js-menu@latest/menu.js"))
  (add-to-list 'org-re-reveal-plugin-config
               '(audioslideshow "RevealAudioSlideshow" "https://cdn.jsdelivr.net/npm/reveal.js-plugins@latest/audio-slideshow/plugin.js"))
  (add-to-list 'org-re-reveal-plugin-config
               '(audio-recorder "RevealAudioRecorder" "https://cdn.jsdelivr.net/npm/reveal.js-plugins@latest/audio-slideshow/recorder.js"))
  (add-to-list 'org-re-reveal-plugin-config
               '(customcontrols "RevealCustomControls" "https://cdn.jsdelivr.net/npm/reveal.js-plugins@latest/customcontrols/plugin.js"))
  (add-to-list 'org-re-reveal-plugin-config
               '(chalkboard "RevealChalkboard" "https://cdn.jsdelivr.net/npm/reveal.js-plugins@latest/chalkboard/plugin.js")))

(use-package org-re-reveal-citeproc :ensure t
  :init
  (defun my-org-re-reveal-citeproc-format-citation (cite-info format)
    "Format a citation using CITE-INFO for FORMAT.
This function is used as a replacement for `org-re-reveal-citeproc-format-citation-function'."
    (let ((formatted (org-re-reveal-citeproc--format-reference cite-info format)))
      (when (eq format 'html)
	(setq formatted (format "<span class=\"cite-reference\">%s</span>" formatted)))
      formatted))
  (advice-add 'org-re-reveal-citeproc-format-citation-function :override #'my-org-re-reveal-citeproc-format-citation)
  :config
  (add-to-list 'org-export-filter-paragraph-functions
               #'org-re-reveal-citeproc-filter-cite))

;; multiplexing

;; Define the publishing project
(setq org-publish-project-alist
      (list
       (list
	"public"
	:base-directory "./source"
	:make-directory t
	:recursive nil
	:publishing-directory "./public"
	:base-extension "org"
	:with-footnotes t
	:publishing-function 'org-re-reveal-publish-to-reveal-client)
       (list
	"local"
	:base-directory "./source"
	:make-directory t
	:recursive nil
	:publishing-directory "./public"
	:base-extension "org"	
	:with-footnotes t
	:publishing-function 'org-re-reveal-publish-to-reveal)
       (list
	"static"
	:base-directory "./source"
	:make-directory t
	:recursive t
	:publishing-directory "./public"
	:base-extension "css\\|js\\|png\\|jpg\\|gif\\|svg\\|pdf\\|wav\\|mp4"
	:publishing-function 'org-publish-attachment)
       (list
	"site-public"
	:components '("public" "static"))
       (list
	"site-local"
	:components '("local" "static"))))


;; No backup files
(setq make-backup-files nil)

